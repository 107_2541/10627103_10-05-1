package com.company;

public class Main {
    public static void main(String[] args) {
        java.util.Scanner scanner =
                new java.util.Scanner(System.in);

        System.out.print("請輸入一個數:");
        int input = scanner.nextInt();

        if (input % 2 == 0)
            System.out.println(input + " 不是奇數(T_T)/~~~");
        else
            System.out.println(input + " 是奇數（＾ω＾）");
    }
}